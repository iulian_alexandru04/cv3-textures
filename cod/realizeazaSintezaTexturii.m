function imgSintetizata = realizeazaSintezaTexturii(parametri)

dimBloc = parametri.dimensiuneBloc;
nrBlocuri = parametri.nrBlocuri;
[H,W,c] = size(parametri.texturaInitiala);
H2 = parametri.dimensiuneTexturaSintetizata(1);
W2 = parametri.dimensiuneTexturaSintetizata(2);
if strcmp(parametri.optiuneTransfer,'da') %suprascrie dimensiuni rezultat
    H2 = size(parametri.imaginePentruTransfer,1);
    W2 = size(parametri.imaginePentruTransfer,2);
else
    parametri.fidelitateTransfer=0;
end
overlap = dimBloc*parametri.portiuneSuprapunere;

% o imagine este o matrice cu 3 dimensiuni: inaltime x latime x nrCanale
% variabila blocuri - matrice cu 4 dimensiuni: punem fiecare bloc (portiune din textura initiala)
% unul peste altul
blocuri = uint8(zeros(dimBloc,dimBloc,c,nrBlocuri));

%selecteaza blocuri aleatoare din textura initiala
%genereaza (in maniera vectoriala) punctul din stanga sus al blocurilor
y = randi(H-dimBloc+1,nrBlocuri,1);
x = randi(W-dimBloc+1,nrBlocuri,1);
%extrage portiunea din textura initiala continand blocul
for i =1:nrBlocuri
    blocuri(:,:,:,i) = parametri.texturaInitiala(y(i):y(i)+dimBloc-1,x(i):x(i)+dimBloc-1,:);
end

imgSintetizata = uint8(zeros(H2,W2,c));
nrBlocuriY = ceil(H2/(dimBloc-overlap));
nrBlocuriX = ceil(W2/(dimBloc-overlap));
HBigger = nrBlocuriY*dimBloc-(nrBlocuriY-1)*overlap;
WBigger = nrBlocuriX*dimBloc-(nrBlocuriX-1)*overlap;
imgSintetizataMaiMare = uint8(zeros(HBigger,WBigger,c));
parametri.imaginePentruTransfer=imresize(parametri.imaginePentruTransfer,[HBigger WBigger]);

if strcmp(parametri.metodaSinteza,'blocuriAleatoare')
    overlap=0;
end

for y=1:nrBlocuriY
    for x=1:nrBlocuriX
        info = cautaBloc(imgSintetizataMaiMare,x,y,blocuri,nrBlocuri,parametri.metodaSinteza,overlap,parametri.imaginePentruTransfer,parametri.fidelitateTransfer);
        imgSintetizataMaiMare=puneBloc(imgSintetizataMaiMare,x,y,blocuri,info.indice,overlap,info.drumStanga,info.drumSus);
    end
end

imgSintetizata = imgSintetizataMaiMare(1:H2,1:W2,:);
figure, imshow(parametri.texturaInitiala)
figure, imshow(imgSintetizata);
title(['Rezultat obtinut cu' parametri.metodaSinteza]);
