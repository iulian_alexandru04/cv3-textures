function coordonate = coordonateImg(x,y,dimBloc,overlap,parte)
%returneaza coordonatele fasiei de overlap ale blocului de pe pozitia (y,x)
%din imaginea rezultat tinand cont de partea specificata
%optiuni posibile pentru parte: 'jos' sau 'dreapta'
    coordonate.Xend=int16(x*dimBloc-(x-1)*overlap);
    coordonate.Yend=int16(y*dimBloc-(y-1)*overlap);
    if strcmp(parte,'jos')
        coordonate.Xstart=int16(coordonate.Xend-dimBloc+1);
        coordonate.Ystart=int16(coordonate.Yend-overlap+1);
    elseif strcmp(parte,'dreapta')
        coordonate.Xstart=int16(coordonate.Xend-overlap+1);
        coordonate.Ystart=int16(coordonate.Yend-dimBloc+1);
    else %pentru transfer textura (da coordonatele unui bloc intreg)
        coordonate.Xstart=int16(coordonate.Xend-dimBloc+1);
        coordonate.Ystart=int16(coordonate.Yend-dimBloc+1);
    end
