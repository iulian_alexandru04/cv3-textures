function coordonate = coordonateBloc(dimBloc,overlap,parte)
%returneaza coordonatele fasiei de overlap din partea specificata a blocului
%coordonatele sunt raportate la blocul
%optiuni posibile pentru parte: 'sus' sau 'stanga'
    coordonate.Ystart=1;
    coordonate.Xstart=1;
    if strcmp(parte,'sus') %fac swap
        coordonate.Yend=overlap;
        coordonate.Xend=dimBloc;
    else
        coordonate.Yend=dimBloc;
        coordonate.Xend=overlap;
    end
