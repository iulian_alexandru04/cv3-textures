function diff = diferenta(img1,img2)
    img1=double(img1);
    img2=double(img2);
    diff=mean(mean(mean(abs(img1-img2))));
