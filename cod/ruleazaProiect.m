%seteaza parametri
img = imread('../data/radishes.jpg');
parametri.texturaInitiala = img;
parametri.imaginePentruTransfer = imread('../data/eminescu.jpg');
parametri.optiuneTransfer = 'da';
parametri.fidelitateTransfer = 0.75;
%ia valori in [0,1]
%unde 0=doar sinteza si 1=mozaic

parametri.dimensiuneTexturaSintetizata = [2*size(img,1) 2*size(img,2)];
parametri.dimensiuneBloc = 36;
parametri.nrBlocuri = 2000;
parametri.eroareTolerata = 0.1; % to do
parametri.portiuneSuprapunere = 1/6;
parametri.metodaSinteza = 'frontieraCostMinim';
%metode posibile: 'blocuriAleatoare','eroareSuprapunere','frontieraCostMinim'

imgSintetizata = realizeazaSintezaTexturii(parametri);
