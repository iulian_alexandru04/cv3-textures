function img_cut = cut(img,coord)
%decupeaza din imaginea img un dreptunghi cu coordonatele specificate
%de parametrul coord
    H=coord.Yend-coord.Ystart+1;
    W=coord.Xend-coord.Xstart+1;
    C=size(img,3);
    img_cut=uint8(zeros(H,W,C));
    img_cut(1:H,1:W,:) = img(coord.Ystart:coord.Yend,coord.Xstart:coord.Xend,:);
