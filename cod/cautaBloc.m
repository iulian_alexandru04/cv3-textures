function info = cautaBloc(img,x,y,blocuri,nrBlocuri,metoda,overlap,imgTransfer,fidelitate)
    dim=size(blocuri,1);
    info.indice = randi(nrBlocuri);
    info.drumStanga=zeros(dim);
    info.drumSus=zeros(dim);
    if strcmp(metoda,'blocuriAleatoare') || overlap == 0
        return;
    end


    up_diff = 0;
    lf_diff = 0;
    trans_diff = 0;
    if y ~= 1 %daca am bloc deasupra initializez eroarea de suprapunere
        sus=cut(img,coordonateImg(x,y-1,dim,overlap,'jos'));
        up_diff = diferenta(sus,cut(blocuri(:,:,:,info.indice),coordonateBloc(dim,overlap,'sus')));
    end
    if x ~= 1 %daca am bloc la dreapta intializez eroarea de suprapunere
        stanga=cut(img,coordonateImg(x-1,y,dim,overlap,'dreapta'));
        lf_diff = diferenta(stanga,cut(blocuri(:,:,:,info.indice),coordonateBloc(dim,overlap,'stanga')));
    end
    if fidelitate ~= 0 %daca vrea sa tranfer textura calculez eroarea de suprapunere peste img sursa
        trans_diff = diferenta(blocuri(:,:,:,info.indice),cut(imgTransfer,coordonateImg(x,y,dim,overlap,'bloc')));
    end
    best_diff=(up_diff+lf_diff)*(1-fidelitate)+trans_diff*fidelitate;
    for i=1:nrBlocuri
        %calculez toate erorile necesare
        if y ~= 1
            up_diff = diferenta(sus,cut(blocuri(:,:,:,i),coordonateBloc(dim,overlap,'sus')));
        end
        if x ~= 1
            lf_diff = diferenta(stanga,cut(blocuri(:,:,:,i),coordonateBloc(dim,overlap,'stanga')));
        end
        if fidelitate ~= 0
            trans_diff = diferenta(blocuri(:,:,:,i),cut(imgTransfer,coordonateImg(x,y,dim,overlap,'bloc')));
        end
        crt_diff = (up_diff+lf_diff)*(1-fidelitate)+trans_diff*fidelitate;
        if crt_diff < best_diff
            best_diff = crt_diff;
            info.indice = i;
        end
    end
    if strcmp(metoda,'eroareSuprapunere')
        return;
    end

    %calculez drumuri doar daca fac costMinim
    if x ~= 1
        info.drumStanga=cautaDrum(img,x,y,blocuri,info.indice,overlap,'stanga');
    end
    if y ~= 1
        info.drumSus=cautaDrum(img,x,y,blocuri,info.indice,overlap,'sus');
    end
