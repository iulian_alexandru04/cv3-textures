function img = puneBloc(img,x,y,blocuri,nrbloc,fasie,drumStanga,drumSus)
%pune blocul nrbloc in img pe pozitia (y,x)
%tinand cont de drumurile delimitatoare
%se considera drumul ca facand parte din ce este deja in img (nu il rescriu)

%coltul dreapta-jos al blocului situat pe poz (y,x) in imaginea img
%este la coordonatele (y*dim-(y-1)*fasie,x*dim-(x-1)*fasie)

%deci coltul stanga-sus al blocului situat pe poz(y,x) in imaginea img
%este situat la ((y-1)*(dim-fasie)+1,(x-1)*(dim-fasie)+1)

%-----coordonatele sunt exprimate sub forma (linie,coloana)
dim=size(blocuri,1);
linieStart=(y-1)*(dim-fasie);
coloanaStart=(x-1)*(dim-fasie);
for i=1:dim
    for j=1:dim
        if i>drumSus(j) && j>drumStanga(i)
            img(linieStart+i,coloanaStart+j,:)=blocuri(i,j,:,nrbloc);
        end
    end
end
