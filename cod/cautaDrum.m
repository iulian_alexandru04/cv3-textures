function drum = cautaDrum(img,x,y,blocuri,indice,overlap,parte)
    dim=size(blocuri,1);
    drum=zeros(dim);
    if strcmp(parte,'sus')
        susImg=cut(img,coordonateImg(x,y-1,dim,overlap,'jos'));
        susBlk=cut(blocuri(:,:,:,indice),coordonateBloc(dim,overlap,'sus'));
        diff=abs(double(rgb2gray(susImg))-double(rgb2gray(susBlk)));
        diff=diff';
    else
        stangaImg=cut(img,coordonateImg(x-1,y,dim,overlap,'dreapta'));
        stangaBlk=cut(blocuri(:,:,:,indice),coordonateBloc(dim,overlap,'stanga'));
        diff=abs(double(rgb2gray(stangaImg))-double(rgb2gray(stangaBlk)));
    end
    for i=2:dim
        for j=1:overlap
            best=diff(i-1,j);
            if j>1 && diff(i-1,j-1)<best
                    best=diff(i-1,j-1);
            end
            if j<overlap && diff(i-1,j+1)<best
                best=diff(i-1,j+1);
            end
        end
    end

    %refac drumul optim
    [val idx]=sort(diff(dim,:));
    col=idx(1);
    drum(dim)=col;

    i=dim-1;
    while i>0
        j=drum(i+1);
        o=0; %optiune
        if j>1 && diff(i+1,j-1)<diff(i+1,j+o)
            o=-1;
        end
        if j<overlap && diff(i+1,j+1)<diff(i+1,j+o)
            o=1;
        end
        drum(i)=j+o;
        i=i-1;
    end
